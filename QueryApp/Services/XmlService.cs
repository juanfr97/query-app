using System.IO;
using System.Xml.Serialization;
using Java.Lang;
using QueryApp.DTOs;

namespace QueryApp.Services
{
    public static class XmlService
    {
        private static string path = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);

        public static bool SaveXML(string fileName, PersonDTO personDto)
        {
            try
            {
                var serializer = new XmlSerializer(typeof(PersonDTO));
                //El usuario no tiene acceso a esta carpeta
                var writer = new StreamWriter(
                    Path.Combine(
                        System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal), 
                        $"{fileName}.xml")
                );
                serializer.Serialize(writer, personDto);
                writer.Close();
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }
        
        public static PersonDTO ReadXML(string fileName)
        {
            try
            {
                var serializer = new XmlSerializer(typeof(PersonDTO));
                var reader = new StreamReader(Path.Combine(path, $"{fileName}.xml"));
                var person = (PersonDTO) serializer.Deserialize(reader);
                reader.Close();
                return new PersonDTO()
                {
                    Name = person.Name,
                    Address = person.Address,
                    Age = person.Age,
                    Amount = person.Amount,
                    Email = person.Email,
                    Image = person.Image
                };
            }
            catch (Exception e)
            {
                return new PersonDTO()
                {
                    Address = "",
                    Amount = 0,
                    Age = 0,
                    Email = "",
                    Image = "",
                    Name = ""

                };
            }
        }
    }
}