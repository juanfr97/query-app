using System.IO;
using Microsoft.Data.Sqlite;
using QueryApp.DTOs;

namespace QueryApp.Services
{
    public class SQLiteService
    {
        string basededatos = "BankDBs.db3";


        public SQLiteService()
        {
        }
        public void ConexionBase()
        {
            var RutaBase = Path.Combine(System.Environment.GetFolderPath
                        (System.Environment.SpecialFolder.Personal), basededatos);
            bool existencia = File.Exists(RutaBase);
            if (!existencia)
            {
                //SqliteConnection.CreateFile(RutaBase);
                var conexion = new SqliteConnection("Data Source=" + RutaBase);
                var sql = "CREATE TABLE People (Id INTEGER PRIMARY KEY AUTOINCREMENT, " +
                          "Name VARCHAR(50), Address VARCHAR(50), Email VARCHAR(50), " +
                          "Age INTEGER, Amount NUMERIC(18,2), Image VARCHAR(500));";

                conexion.Open();
                using (var query = conexion.CreateCommand())
                {
                    query.CommandText = sql;
                    query.ExecuteNonQuery();
                }
                conexion.Close();
            }
        }

        public bool IngresarDatos(PersonDTO person)
        {
            var RutaBase = Path.Combine(System.Environment.GetFolderPath
                (System.Environment.SpecialFolder.Personal), basededatos);
            var conexion = new SqliteConnection("Data Source=" + RutaBase);
            try
            {
                conexion.Open();
                var sql = "INSERT INTO People (Name, Address, Email, Age, Amount, Image)" +
                    "VALUES (@Name, @Address, @Email, @Age, @Amount, @Image);";
                using (var query = conexion.CreateCommand())
                {
                    query.CommandText = sql;
                    query.Parameters.AddWithValue("@Name", person.Name);
                    query.Parameters.AddWithValue("@Address", person.Address);
                    query.Parameters.AddWithValue("@Email", person.Email);
                    query.Parameters.AddWithValue("@Age", person.Age);
                    query.Parameters.AddWithValue("@Amount", person.Amount);
                    query.Parameters.AddWithValue("@Image", person.Image);
                    query.ExecuteNonQuery();
                }
                conexion.Close();
                return true;
            }
            catch (System.Exception ex)
            {
                conexion.Close();
                return false;
            }
        }

        public PersonDTO Buscar(int id)
        {
            var RutaBase = Path.Combine(System.Environment.GetFolderPath
                (System.Environment.SpecialFolder.Personal), basededatos);
            var conexion = new SqliteConnection("Data Source=" + RutaBase);
            var person = new PersonDTO()
            {
                Address = "",
                Amount = 0,
                Age = 0,
                Email = "",
                Image = "",
                Name = ""
            };
            try
            {
                conexion.Open();
                using (var contenido = conexion.CreateCommand())
                {
                    contenido.CommandText = "SELECT * FROM People WHERE Id=" + id + ";";
                    var lectura = contenido.ExecuteReader();
                    while (lectura.Read())
                    {
                        person.Name = lectura[1].ToString();
                        person.Address = lectura[2].ToString();
                        person.Email = lectura[3].ToString();
                        person.Age = int.Parse(lectura[4].ToString());
                        person.Amount = double.Parse(lectura[5].ToString());
                        person.Image = lectura[6].ToString();
                    }
                }
                conexion.Close();
                return person;
            }
            catch (System.Exception ex)
            {
                conexion.Close();
                return person;
            }
        }
    }
}