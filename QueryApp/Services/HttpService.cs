using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;
using QueryApp.DTOs;

namespace QueryApp.Services
{
    public class HttpService
    {
        private readonly HttpClient _client;
        private static string url = "https://bankapi.juanfrausto.com/api/People";

        public HttpService()
        {
            _client = new HttpClient();
        }

        public async Task<PersonDTO> GetPersonAsync(int id)
        {
            var response = await _client.GetAsync($"{url}/{id}");
            var content = await response.Content.ReadAsStringAsync();
            var person = JsonConvert.DeserializeObject<PersonDTO>(content);
            if (person is null)
            {
                return new PersonDTO()
                {
                    Address = "",
                    Amount = 0,
                    Age = 0,
                    Email = "",
                    Image = "",
                    Name = ""

                };
            }
            return person;
        }

        public async Task<List<PersonDTO>> GetPeopleAsync()
        {
            var response = await _client.GetAsync(url);
            var content = await response.Content.ReadAsStringAsync();
            var people = JsonConvert.DeserializeObject<List<PersonDTO>>(content);
            return people;
        }

        public async Task<bool> CreatePersonAsync(PersonDTO person)
        {
            var json = JsonConvert.SerializeObject(person);
            var data = new StringContent(json, System.Text.Encoding.UTF8, "application/json");
            var response = await _client.PostAsync(url, data);
            return response.IsSuccessStatusCode;
        }


    }
}