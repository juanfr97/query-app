﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Xml.Serialization;
using Android.App;
using Android.OS;
using Android.Runtime;
using Android.Views;
using AndroidX.AppCompat.Widget;
using AndroidX.AppCompat.App;
using Google.Android.Material.FloatingActionButton;
using Google.Android.Material.Snackbar;
using Android.Widget;
using Java.Net;
using Newtonsoft.Json;
using QueryApp.Services;
using QueryApp.DTOs;
using Android.Graphics;
using System.Net;
using Toolbar = Android.Widget.Toolbar;

namespace QueryApp
{
    [Activity(Label = "@string/app_name", Theme = "@style/AppTheme.NoActionBar", MainLauncher = true)]
    public class MainActivity : AppCompatActivity
    {
        ImageView imageView;
        EditText editTxtName, editTxtAge, editTextAdd, editTxtEmail, editTxtAmount, editTxtSearchid, editTxtImg;
        Button btnSaveXml, btnSearchXml, btnSearchApi, btnSaveApi, btnClear, btnSaveSql, btnSearchSql;
        HttpClient client = new HttpClient();
        SQLiteService sQLiteService;
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.activity_main);
            sQLiteService = new SQLiteService();
            sQLiteService.ConexionBase();
            imageView = FindViewById<ImageView>(Resource.Id.img);
            editTxtName = FindViewById<EditText>(Resource.Id.editTxtName);
            editTextAdd = FindViewById<EditText>(Resource.Id.editTxtAdd);
            editTxtAge = FindViewById<EditText>(Resource.Id.editTxtAge);
            editTxtEmail = FindViewById<EditText>(Resource.Id.editTxtEmail);
            editTxtAmount = FindViewById<EditText>(Resource.Id.editTxtAmount);
            editTxtSearchid = FindViewById<EditText>(Resource.Id.editTxtId);
            editTxtImg = FindViewById<EditText>(Resource.Id.editTxtImg);

            btnSaveXml = FindViewById<Button>(Resource.Id.btnSaveXml);
            btnSearchXml = FindViewById<Button>(Resource.Id.btnSearchXml);

            btnSearchApi = FindViewById<Button>(Resource.Id.btnSearchApi);
            btnSaveApi = FindViewById<Button>(Resource.Id.btnSaveApi);

            btnSaveSql = FindViewById<Button>(Resource.Id.btnSaveSql);
            btnSearchSql = FindViewById<Button>(Resource.Id.btnSearchSql);

            btnClear = FindViewById<Button>(Resource.Id.btnClear);

            btnSaveXml.Click += BtnSaveXml_Click;
            btnSearchXml.Click += BtnSearchXml_Click;
            btnSearchApi.Click += BtnSearchSql_Click;
            btnSaveApi.Click += BtnSaveSql_Click;
            btnClear.Click += BtnClear_Click;
            btnSaveSql.Click += BtnSaveSqlite_Click;
            btnSearchSql.Click += BtnSearchSqlite_Click;

        }

        private void BtnSearchSqlite_Click(object sender, EventArgs e)
        {
            try
            {
                var id = int.Parse(editTxtSearchid.Text);
                var person = sQLiteService.Buscar(id);
                var imageBitMap = GetImageBitmapFromUrl(person.Image);
                editTxtName.Text = person.Name;
                editTextAdd.Text = person.Address;
                editTxtAge.Text = person.Age.ToString();
                editTxtEmail.Text = person.Email;
                editTxtAmount.Text = person.Amount.ToString();
                editTxtImg.Text = person.Image;
                imageView.SetImageBitmap(imageBitMap);
                Toast.MakeText(this, "Registro traido correctamente desde SQLite", ToastLength.Short).Show();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        private void BtnSaveSqlite_Click(object sender, EventArgs e)
        {
            try
            {
                var person = new PersonDTO()
                {
                    Name = editTxtName.Text,
                    Address = editTextAdd.Text,
                    Age = int.Parse(editTxtAge.Text),
                    Email = editTxtEmail.Text,
                    Amount = double.Parse(editTxtAmount.Text),
                    Image = editTxtImg.Text
                };

                sQLiteService.IngresarDatos(person);
                Toast.MakeText(this, "Texto guardado correctamente en XML", ToastLength.Short).Show();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                Toast.MakeText(this, "Error SQLite", ToastLength.Short).Show();
            }

        }

        private void BtnClear_Click(object sender, EventArgs e)
        {
            editTxtName.Text = string.Empty;
            editTextAdd.Text = string.Empty;
            editTxtAge.Text = string.Empty;
            editTxtEmail.Text = string.Empty;
            editTxtAmount.Text = string.Empty;
            editTxtImg.Text = string.Empty;
            imageView.SetImageBitmap(null);
        }

        private async void BtnSaveSql_Click(object sender, EventArgs e)
        {
            var httpService = new HttpService();
            try
            {
                var person = new PersonDTO()
                {
                    Name = editTxtName.Text,
                    Address = editTextAdd.Text,
                    Age = int.Parse(editTxtAge.Text),
                    Email = editTxtEmail.Text,
                    Amount = double.Parse(editTxtAmount.Text),
                    Image = editTxtImg.Text
                };
                var json = JsonConvert.SerializeObject(person);
                var content = new StringContent(json, System.Text.Encoding.UTF8, "application/json");
                var response = await httpService.CreatePersonAsync(person);
                if (response)
                {
                    Toast.MakeText(this, "Registro guardado correctamente en la API", ToastLength.Short).Show();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                Toast.MakeText(this, "Error API", ToastLength.Short).Show();
            }
        }

        private async void BtnSearchSql_Click(object sender, EventArgs e)
        {
            var httpService = new HttpService();
            try
            {
                var id = int.Parse(editTxtSearchid.Text);
                var person = await httpService.GetPersonAsync(id);
                var imageBitMap = GetImageBitmapFromUrl(person.Image);
                editTxtName.Text = person.Name;
                editTextAdd.Text = person.Address;
                editTxtAge.Text = person.Age.ToString();
                editTxtEmail.Text = person.Email;
                editTxtAmount.Text = person.Amount.ToString();
                editTxtImg.Text = person.Image;
                imageView.SetImageBitmap(imageBitMap);
                Toast.MakeText(this, "Registro traido correctamente desde la API", ToastLength.Short).Show();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        private void BtnSearchXml_Click(object sender, EventArgs e)
        {
            var person = new PersonXMLDTO();
            try
            {
                person.Id = int.Parse(editTxtSearchid.Text);
                var serializer = new XmlSerializer(typeof(PersonXMLDTO));
                var reader = new StreamReader(
                    System.IO.Path.Combine(
                        System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal),
                        $"{editTxtSearchid.Text}.xml")
                    );
                var datos = (PersonXMLDTO)serializer.Deserialize(reader);
                reader.Close();
                editTxtName.Text = datos.Name;
                editTextAdd.Text = datos.Address;
                editTxtAge.Text = datos.Age.ToString();
                editTxtEmail.Text = datos.Email;
                editTxtAmount.Text = datos.Amount.ToString();
                editTxtImg.Text = datos.Image;
                imageView.SetImageBitmap(GetImageBitmapFromUrl(datos.Image));
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception);
            }
        }

        private void BtnSaveXml_Click(object sender, EventArgs e)
        {
            var person = new PersonXMLDTO();
            try
            {
                person.Id = int.Parse(editTxtSearchid.Text);
                person.Name = editTxtName.Text;
                person.Address = editTextAdd.Text;
                person.Age = int.Parse(editTxtAge.Text);
                person.Email = editTxtEmail.Text;
                person.Amount = double.Parse(editTxtAmount.Text);
                person.Image = editTxtImg.Text;
                var serializer = new XmlSerializer(typeof(PersonXMLDTO));
                //El usuario no tiene acceso a esta carpeta
                var writer = new StreamWriter(
                    System.IO.Path.Combine(
                        System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal),
                        $"{editTxtSearchid.Text}.xml")
                    );
                serializer.Serialize(writer, person);
                writer.Close();
                Toast.MakeText(this, "Texto guardado correctamente en XML", ToastLength.Short).Show();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                Toast.MakeText(this, "Error XML", ToastLength.Short).Show();
            }
        }

        private Bitmap GetImageBitmapFromUrl(string url)
        {
            Bitmap imageBitmap = null;

            using (var webClient = new WebClient())
            {
                var imageBytes = webClient.DownloadData(url);
                if (imageBytes != null && imageBytes.Length > 0)
                {
                    imageBitmap = BitmapFactory.DecodeByteArray(imageBytes, 0, imageBytes.Length);
                }
            }

            return imageBitmap;
        }

    }
}
