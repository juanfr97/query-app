namespace QueryApp.DTOs
{
    public class PersonDTO
    {

        public string Name { get; set; }
        public string Address { get; set; }
        public string Email { get; set; }
        public int Age { get; set; }
        public double Amount { get; set; }
        public string Image { get; set; }
    }
}