using Android.App;
using Android.OS;
using AndroidX.AppCompat.App;

namespace QueryApp.Views
{
    [Activity(Label = "@string/app_name", Theme = "@style/AppTheme.NoActionBar", MainLauncher = false)]
    public class TestActivity: AppCompatActivity
    {
        
         override void OnCreate(Bundle savedInstanceState, PersistableBundle persistentState)
        {
            base.OnCreate(savedInstanceState, persistentState);
        }
    }
}